/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环控制所有的回合
    for (int i = 0; i < n - 1; i++) {
        // 内层循环控制每一轮的冒泡处理
        for (int j = 0; j < n - 1 - i; j++) {
            // 相邻元素进行比较，如果逆序则交换
            if (a[j] > a[j + 1]) {
                // 交换a[j]和a[j+1]
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} // end
