/**  
 * 冒泡排序函数  
 * 通过比较相邻的元素并交换它们的位置，如果它们的顺序错误就把它们交换过来。  
 * 遍历数列的工作是重复地进行直到没有再需要交换，也就是说该数列已经排序完成。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 用于标记数组是否已经有序，如果某次循环中没有发生交换，则说明已经有序  
        boolean isSorted = true;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换元素  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记数组在本次循环中发生了交换，因此数组还未有序  
                isSorted = false;  
            }  
        }  
        // 如果在一次循环中没有发生任何交换，则数组已经有序，可以提前结束排序  
        if (isSorted) {  
            break;  
        }  
    }  
} //end
