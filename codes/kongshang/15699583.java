/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    // 标记变量，用于优化冒泡排序，如果一趟排序中没有发生交换，则说明数组已经有序，可以提前结束排序
    boolean swapped;
    // 外层循环控制遍历次数
    for (int i = 0; i < n - 1; i++) {
        swapped = false;
        // 内层循环负责进行相邻元素的比较和可能的交换
        for (int j = 0; j < n - 1 - i; j++) {
            // 比较相邻的两个元素
            if (a[j] > a[j + 1]) {
                // 如果前一个元素比后一个元素大，则交换它们的位置
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                // 标记发生了交换
                swapped = true;
            }
        }
        // 如果在这次遍历中没有发生交换，说明数组已经有序，可以提前结束排序
        if (!swapped) {
            break;
        }
    }
} //end
