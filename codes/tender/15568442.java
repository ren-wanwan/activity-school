/**  
 * 冒泡排序函数  
 * 通过重复地遍历要排序的数组，比较每对相邻的元素，并在必要时交换它们的位置，  
 * 遍历数组的工作是重复地进行直到没有再需要交换，也就是说该数组已经排序完成。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 创建一个标志位，用于检测这一趟是否有交换过  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标志位设为 true，表示这一趟发生了交换  
                swapped = true;  
            }  
        }  
        // 如果这一趟没有发生交换，说明数组已经有序，可以提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
