/**
 * 冒泡排序函数
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 遍历数组的所有元素
    for (int i = 0; i < n - 1; i++) {
        // 从第一个元素开始，比较相邻的两个元素，如果前一个元素大于后一个元素，则交换它们的位置
        for (int j = 0; j < n - 1 - i; j++) {
            if (a[j] > a[j + 1]) {
                // 交换 a[j] 和 a[j + 1] 的值
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} //end
