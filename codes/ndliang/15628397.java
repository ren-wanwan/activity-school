
/**  
 * 冒泡排序函数  
 * 这是一个经典的排序算法，通过不断比较相邻的元素，将较大的元素往后移动，实现数组的升序排序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制所有元素是否都排好序  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环负责两两比较  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
