/**  
 * 冒泡排序函数  
 * 功能：对输入的整数数组进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    // 外层循环控制所有元素是否已排序  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环负责两两相邻元素比较，并根据大小交换位置  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
