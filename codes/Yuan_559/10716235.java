/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for(int aa=0; aa<n-1; aa++){
    	for(int bb=0; bb<n-1-aa; bb++){
	    if(a[bb]>a[bb+1]) {
		int cc = a[bb];
		a[bb] = a[bb+1];
		a[bb+1] = cc;
	    }
	}
    }
} //end
