/**
 * 冒泡排序函数
 * 对给定数组a进行升序排序
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    for (int i = 0; i < n - 1; i++) { // 外层循环控制比较轮数
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制每轮比较次数
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，交换它们的位置
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} // end
